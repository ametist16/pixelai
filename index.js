const fs = require('fs');
const express = require('express')
const path = require('path');
const app = express()
const port = 3003

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname+'/view.html'))
})

app.get('/get', (req, res) => {
	res.send(check(req.query.data))
})
app.get('/add', (req, res) => {
	res.send(add(req.query.data, req.query.value))
})

app.listen(port, () => {
	console.log('listen')
})

const check = (data) => {
	const content = fs.readFileSync('./data.txt', 'utf8')
	const base = content.trim().split('\n').map(point => point.split(':'))
	const points = {}

	data = data.split(',')

	base.forEach(point => {
		const key = point[0]
		const value = point[1]
		let equal = 0

		value.split(',').forEach((p, i) => {
			if (p === data[i]) {
				equal++
			}
		})

		points[key] = points[key] || []
		points[key].push(equal)
	})

	const result = {}

	for (const key in points) {
		result[key] = points[key].reduce((pixel, total) => total + pixel, 0) / points[key].length
	}

	const total = Object.values(result).reduce((pixel, total) => total + pixel, 0)

	let max = 0
	let maxKey = ''

	for (const key in result) {
		const value = 100 / total * result[key]
		result[key] = value

		if (value > max) {
			max = value
			maxKey = key
		}
	}

	return {
		value: maxKey,
		all: result
	}
}

const add = (data, value) => {
	return fs.appendFileSync('./data.txt', `\n${value}:${data}`);
}
